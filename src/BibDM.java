import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Hashtable;

public class BibDM{

    public static Integer plus(Integer a, Integer b){
        return a+b;
    }

    public static Integer min(List<Integer> liste){
        Integer mini = null;
        if (liste.isEmpty()){
            return null;
        }
        else{
            mini = liste.get(0);
            for (int elem : liste){
                if (elem < mini){
                    mini = elem;
                }
            }
        }
        return mini;
    }
    
    
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        for ( T val : liste){
            if (val.compareTo(valeur) <=0){
                return false;
            }
        }
        return true;

    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        List<T> listeInter = new ArrayList<>();
        int compteur1 = 0;
        int compteur2 = 0;
        while (compteur1 < liste1.size() && compteur2 < liste2.size()) {
            T objet = liste1.get(compteur1);
            int compare = objet.compareTo(liste2.get(compteur2));
            if (compare == 0) {
                if (!listeInter.contains(objet))
                    listeInter.add(objet);
                compteur1++;
                compteur2++;
            } else if (compare > 0) {
                compteur2++;
            } else {
                compteur1++;
            }
        }
        return listeInter;

    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        String[] mots = null;
        mots = texte.split(" ");
        List<String> liste = new ArrayList<>();
        for (String mot : mots)
            if (!mot.equals(""))
                liste.add(mot);
        return liste;

    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        List<String> mots = decoupe(texte);
        Hashtable<String, Integer> frequence = new Hashtable<>();
        for (String mot : mots) {
            if (frequence.contains(mot))
                frequence.put(mot, frequence.get(mot) + 1);
            else
                frequence.put(mot, 1);
        }
        String motMax = null;
        for (String mot : frequence.keySet()) {
            if (motMax == null || frequence.get(mot) > frequence.get(motMax))
                motMax = mot;
            else if (frequence.get(mot) == frequence.get(motMax))
                if (mot.compareTo(motMax) < 0)
                    motMax = mot;
        }
        return motMax;

    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        int equilibre = 0;
        for (int i =0; i < chaine.length(); i++){
            Character val = chaine.charAt(i);
            if (equilibre < 0){return false;}
            if (val == '('){
                equilibre++;
            }else if (val == ')'){
                equilibre--;
            }
        }
        return (equilibre==0);

    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        List<Character> lstOpen = new ArrayList<>();
        char charI;
        for(int i = 0; i < chaine.length(); i++)
        {
            charI = chaine.charAt(i);
            if(charI == '(' || charI == '[')
                lstOpen.add(charI);
            else if(charI == ']')
            {
                if(lstOpen.size() == 0 || !lstOpen.get(lstOpen.size() - 1).equals('['))
                    return false;
                else
                    lstOpen.remove(lstOpen.size() - 1);
            }
            else if(charI == ')')
            {
                if(lstOpen.size() == 0 || !lstOpen.get(lstOpen.size() - 1).equals('('))
                    return false;
                else
                    lstOpen.remove(lstOpen.size() - 1);
            }
        }
        return lstOpen.size() == 0;
    }

    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        if (liste.isEmpty())
            return false;
        int borneInf = 0;
        int borneSup = liste.size() - 1;
        int milieu = (borneInf + borneSup) / 2;
        while (borneInf <= borneSup) {
            if (liste.get(milieu) == valeur)
                return true;
            if (liste.get(milieu) < valeur)
                borneInf = milieu + 1;
            else
                borneSup = milieu - 1;
            milieu = (borneInf + borneSup) / 2;
        }
        return false;
    }
}
